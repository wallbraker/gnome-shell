#!/bin/bash

mkdir -p deps
cd deps

git clone https://gitlab.freedesktop.org/xrdesktop/g3k.git
cd g3k
git checkout $1
meson build --prefix /usr -Dshaderc_link_combined=true
ninja -C build install
cd ../..
